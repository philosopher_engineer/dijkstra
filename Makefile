CC = gcc
CFLAGS = -g -std=c99 -Wall -Wextra -Werror -fprofile-arcs -ftest-coverage
LDFLAGS = -g -lgcov --coverage

PROG = program
HDRS = graphs.h dijkstra.h
SRCS = main.c graphs.c dijkstra.c

OBJS = $(SRCS:.c=.o)

$(PROG) : $(OBJS)
	$(CC) $(LDFLAGS) $(OBJS) -o $(PROG)

main.o: graphs.h dijkstra.h main.c
graphs.o: graphs.c graphs.h
dijkstra.o: dijkstra.c dijkstra.h

clean:
	rm -rf core $(PROG) $(OBJS)

TAGS: $(SRCS) $(HDRS)
	etags -t $(SRCS) $(HDRS)
