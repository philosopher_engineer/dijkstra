#include "dijkstra.h"

int main() {
    graph_t G;
    GRAPH_create_empty_graph(&G, 6);

    // Add edges to create full graph
    // Vertex 1
    GRAPH_add_edge(&G, 1, 2, 7);
    GRAPH_add_edge(&G, 1, 3, 9);
    GRAPH_add_edge(&G, 1, 6, 14);

    // Vertex 2
    GRAPH_add_edge(&G, 2, 1, 7);
    GRAPH_add_edge(&G, 2, 3, 10);
    GRAPH_add_edge(&G, 2, 3, 80); /* Bad design - Catch it! */
    GRAPH_add_edge(&G, 2, 4, 15);

    // Vertex 3
    GRAPH_add_edge(&G, 3, 1, 9);
    GRAPH_add_edge(&G, 3, 2, 10);
    GRAPH_add_edge(&G, 3, 4, 11);
    GRAPH_add_edge(&G, 3, 6, 2);

    // Vertex 4
    GRAPH_add_edge(&G, 4, 2, 15);
    GRAPH_add_edge(&G, 4, 3, 11);
    GRAPH_add_edge(&G, 4, 5, 6);

    // Vertex 5
    GRAPH_add_edge(&G, 5, 4, 6);
    GRAPH_add_edge(&G, 5, 6, 9);

    // Vertex 6
    GRAPH_add_edge(&G, 6, 1, 14);
    GRAPH_add_edge(&G, 6, 3, 2);
    GRAPH_add_edge(&G, 6, 5, 9);
    
    /* Do dummy things for now */
    printf("Number of vertices is %d\n", GRAPH_get_number_of_vertices(&G));
    printf("Number of edges is %d\n", GRAPH_get_number_of_edges(&G));

    graph_vertex_id_t source = 1;
    graph_vertex_id_t target = 5;
    graph_vertex_id_t currentNode = source;

    /* DIJKSTRA Algorithm Mechanics */

    algo_node_ptr_t algo_node_table = DIJKSTRA_algo_node_table_create(source, G.nodes);

    while (currentNode != target) {
        DIJKSTRA_algo_visit_node(currentNode, &G, algo_node_table);
        currentNode = DIJKSTRA_algo_find_best_neighbor_of_visited_node(currentNode, &G, algo_node_table);
    }

    DIJKSTRA_algo_print_path(algo_node_table, source, target);
    printf("For target node cost from source is: %d\n", algo_node_table[target - 1].cost_from_source);

    GRAPH_destroy(&G);
    free(algo_node_table);
	
    return 0;
}
